"use strict";

let OPTS = {
    size: -42, // should be set by onload to be equal to canvas.width
    border: 25,
    borderForce: 0.05,
    max: 10,
    displayQuads: false,
    displayQuadFill: false,
    displayBoids: true,
    debug: false,
    repulse: true,
    run: true,
    boids: 1000,

    speed: 30,
    SEP: 32,
    ALI: 5,
    COH: 1.4,
    DIS: 30
};

class Vec {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    copy() {
	return new Vec(this.x, this.y);
    }
    
    isIn(box) {
        return (box.x-box.w <= this.x && box.x+box.w > this.x &&
	        box.y-box.h <= this.y && box.y+box.h > this.y);
    }

    set(x, y) {
        this.x = x;
        this.y = y;
        return this;
    }

    norm() {
        return Math.sqrt(this.x*this.x + this.y*this.y);
    }

    normalize() {
        const d = this.norm();
        if (d) {
            this.x /= d;
            this.y /= d;
        }
        return this;
    }

    add(x, y) {
        this.x += x;
        this.y += y;
        return this;
    }

    mul(f) {
        this.x *= f;
        this.y *= f;
        return this;
    }

    dot(p) {
	return this.x*p.x + this.y*p.y;
    }
}

class Quad {
    constructor(x, y, w, h, parent=null) {
        this.box = { x, y, w, h };
	this.elems = [];
	this.subs = null;
	this.parent = parent;
        this.color = `rgba(0,0,255,${1/Math.sqrt(this.box.w*this.box.h)})`;
    }

    contains(p) {
        return p.isIn(this.box);
    }

    overlap(box) {
	return !(box.x-box.w > this.box.x+this.box.w ||
		 box.x+box.w < this.box.x-this.box.w ||
		 box.y-box.h > this.box.y+this.box.h ||
		 box.y+box.h < this.box.y-this.box.h);

    }

    add(e) {
	if (this.elems) {
	    e.quad = this;
	    this.elems.push(e);
	    if (this.elems.length > OPTS.max) {
		this.split();
		this.dispatch(this.elems);
		this.elems = null;
	    }
	} else {
	    if (this.contains(e.p)) {
		this.dispatch([ e ]);
	    } else {
		this.parent.add(e);
		if (this.subs) {
		    this.tryCollapse();
		}
	    }
	}
    }

    split() {
	const s = this.box.w/2;
	this.subs = [
	    new Quad(this.box.x-s, this.box.y-s, s, s, this),
	    new Quad(this.box.x+s, this.box.y-s, s, s, this),
	    new Quad(this.box.x-s, this.box.y+s, s, s, this),
	    new Quad(this.box.x+s, this.box.y+s, s, s, this)
	];
    }

    dispatch(elems) {
	for(const e of elems) {
            let i;
	    for(const s of this.subs) {
		if (s.contains(e.p)) {
		    s.add(e);
		    break;
		}
		i++;
	    }
	    if (i === this.subs.length) {
		if (this.parent) {
		    this.parent.add(e);
		} else {
		    console.log("ERROR! Doesn't fit!");
		}
	    }
	}	
    }
    
    tryCollapse() {
	let sum = 0;
	for(const s of this.subs) {
            if (!s.elems || sum >= OPTS.max) {
                return;
            }
	    sum += s.elems;
	}
	if (sum < OPTS.max) {
            this.collapse();
	    if (this.parent) {
		this.parent.tryCollapse();
	    }
	}
    }

    collapse() {
	let elems = [...this.subs[0].elems,
		     ...this.subs[1].elems,
		     ...this.subs[2].elems,
		     ...this.subs[3].elems ];
	this.subs = null;
	for(const e in elems) {
	    e.quad = this;
	}
	this.elems = elems;
    }

    update(e) {
	if (this.contains(e.p) == false) {
	    this.elems = this.elems.filter(i => i != e );
	    this.parent.add(e);
	}
    }

    queryCall(box, fct) {
	if (this.elems) {
	    for (const e of this.elems) {
		if (e.p.isIn(box)) {
		    fct(e);
		}
	    }
	} else {
	    for (const s of this.subs) {
                if (s.overlap(box)) {
		    s.queryCall(box, fct);
                }
	    }
	}
    }

    query(box) {
	if (this.elems) {
	    return this.elems.filter((e) => e.p.isIn(box));
	} else {
	    return this.subs.reduce((acc, s) => acc.concat(s.query(box)), []);
	}
    }

    // another query implementation, just less functional
    query1(box) {
	let elems = [];
	if (this.elems) {
	    this.elems.forEach(e => {
		if (e.p.isIn(box)) {
		    elems.push(e);
		}
	    });
	} else {
	    this.subs.forEach(s => {
		elems = elems.concat(s.query1(box));
	    });
	}
	return elems;
    }
        
    draw(ctx, stroke, fill) {
        if (fill) {
	    ctx.fillStyle = this.color;
	    ctx.fillRect(this.box.x-this.box.w, this.box.y-this.box.h,
                         this.box.w*2, this.box.h*2);
        }

	if (this.subs) {
	    this.subs.forEach((s) => s.draw(ctx, stroke, fill));
	}

        if (stroke) {
	    ctx.strokeRect(this.box.x-this.box.w, this.box.y-this.box.h,
                           this.box.w*2, this.box.h*2);
        }

    }
}

class Flock {
    constructor(maxX, maxY, sprite=null) {
	this.sprite = sprite;
	this.debug = false;
	this.p = new Vec(Math.random()*maxX, Math.random()*maxY);
	this.v = (new Vec(-0.5+Math.random(),-0.5+Math.random())).normalize();
        this.s = new Vec(0, 0);
    }

    repulse(x, y) {
        this.v.set(this.p.x-x,this.p.y-y).normalize();
    }

    impulse(x, y) {
        this.v.add(x, y).normalize();
    }

    dir(q) {
	let sep = new Vec(0, 0);
	let ali = new Vec(0, 0);
	let coh = new Vec(0, 0);
        let count = 0;

	if (this.debug) {
	    this.debugLst = [];
	}
        let box = { x: this.p.x, y: this.p.y, w: OPTS.DIS, h: OPTS.DIS };
        q.queryCall(box, (e) => {
	    if (e === this) {
                return;
            }

            const diffP = new Vec(e.p.x-this.p.x, e.p.y-this.p.y);

	    const a = this.v.dot(diffP.copy().normalize());
	    if (a < -0.75) {
		return;
	    }

	    const d = diffP.norm();
	    if (d > OPTS.DIS) {
		return;
	    }

	    if (this.debug) {
		this.debugLst.push(e);
	    }
	    sep.add(-diffP.x/d, -diffP.y/d);
	    ali.add(e.v.x, e.v.y);
	    coh.add(diffP.x, diffP.y);
	    count++;
	});

	if (count === 0) {
	    return;
	}

	sep.mul(OPTS.SEP / count);
	ali.mul(OPTS.ALI / count);
	coh.mul( OPTS.COH / count);

        this.s.set(sep.x+ali.x+coh.x, sep.y+ali.y+coh.y).normalize();
    }

    step(dt) {
        this.v.add(this.s.x*dt*5, this.s.y*dt*5).normalize();

        if (this.p.x < OPTS.border) {
            this.impulse(OPTS.borderForce, 0);
        } else if (this.p.x > OPTS.size-OPTS.border) {
            this.impulse(-OPTS.borderForce, 0);
        }
        if (this.p.y < OPTS.border) {
            this.impulse(0, OPTS.borderForce);
        } else if (this.p.y > OPTS.size-OPTS.border) {
            this.impulse(0, -OPTS.borderForce);
        }

	let u = this.v.x * OPTS.speed * dt;
	let v = this.v.y * OPTS.speed * dt;
	if (this.p.x+u <= 0 || this.p.x+u >= OPTS.size) {
	    u = -u;
	    this.v.x *= -1;
	    this.p.x = Math.max(Math.min(this.p.x, OPTS.size), 0);
	}
	if (this.p.y+v <= 0 || this.p.y+v >= OPTS.size) {
	    v = -v;
	    this.v.y *= -1;
	    this.p.y = Math.max(Math.min(this.p.y, OPTS.size), 0);
	}
	this.p.x += u;
	this.p.y += v;
        this.quad.update(this);
    }

    draw(ctx) {
	let a = Math.atan2(this.v.y, this.v.x);

	ctx.save();
	ctx.translate(this.p.x, this.p.y);
	ctx.rotate(a);
	ctx.drawImage(this.sprite, -this.sprite.width/2, -this.sprite.height/2);
	ctx.restore();

	if (this.debug === true && this.debugLst) {
	    ctx.beginPath();
	    this.debugLst.forEach((e) => {
		ctx.moveTo(this.p.x, this.p.y);
		ctx.lineTo(e.p.x, e.p.y);
	    });
	    ctx.stroke();
	}
    }
}

const makeSprite = () => {
    let rdr = document.createElement("canvas");
    rdr.width=20;
    rdr.height=20;
    const ctx2 = rdr.getContext("2d");
    ctx2.translate(10, 10);
    ctx2.beginPath();
    ctx2.moveTo(2.5,0);
    ctx2.lineTo(-2.5,2);
    ctx2.lineTo(-2.5,-2);
    ctx2.lineTo(2.5,0);
    ctx2.stroke();
    return rdr;
};

const genBoids = (n, q, sprite) => {
    let lst = [];

    for(let i=0; i < n; i++) {
	let f = new Flock(OPTS.size, OPTS.size, sprite);
	q.add(f);
	lst.push(f);
    }
    return lst;
};

const initUi = (canvas, mouse, sprite, env) => {
    let checks = document.querySelectorAll('input[type="checkbox"]');
    for(const box of checks) {
	box.checked = OPTS[box.dataset.name];
	box.onclick = () => {
	    OPTS[box.dataset.name] = box.checked;
	};
    }

    let ranges = document.querySelectorAll('input[type="range"]');
    for(const box of ranges) {
	box.value = OPTS[box.dataset.name];
	box.onchange = () => {
            console.log(box.dataset.name, "=", box.value);
	    OPTS[box.dataset.name] = box.value;
	};
    }

    let clearDebug = document.getElementById("clearDebug");
    clearDebug.onclick = () => env.lst.forEach((e) => e.debug = false);

    let numbers = document.querySelectorAll('input[type="number"]');
    for(const input of numbers) {
        input.value = OPTS[input.dataset.name];
        input.onchange = () => {
            OPTS[input.dataset.name] = parseInt(input.value);
            if (input.dataset.name === "boids") {
	        env.q = new Quad(OPTS.size/2, OPTS.size/2,
                                 OPTS.size/2, OPTS.size/2);
	        env.lst = genBoids(parseInt(OPTS.boids), env.q, sprite);
            }
        };
    }

    canvas.onmousemove = (evt) => {
        const bbox = evt.currentTarget.getBoundingClientRect();
	mouse.x = evt.clientX - bbox.x;
	mouse.y = evt.clientY - bbox.y;
    };
    canvas.onmousedown = () => mouse.button = true;
    canvas.onmouseup = () => mouse.button = false;
    canvas.onmouseout = () => {
	mouse.x = -OPTS.size;
	mouse.y = -OPTS.size;
    };
};

const draw = (ctx, q, mouse, fps) => {
    ctx.clearRect(0, 0, OPTS.size, OPTS.size);
    ctx.strokeStyle = "#000000";
    
    if (OPTS.displayQuads === true || OPTS.displayQuadFill === true) {
	q.draw(ctx, OPTS.displayQuads, OPTS.displayQuadFill);
    }
    
    if (OPTS.displayBoids) {
	q.queryCall(q.box, (e) => e.draw(ctx));
    }
    
    if (mouse.x >= 0 && mouse.y >= 0) {
	ctx.strokeStyle = "#FF0000";
        ctx.strokeRect(mouse.x-mouse.w, mouse.y-mouse.h, mouse.w*2,mouse.h*2);
	if (mouse.button) {
	    ctx.fillStyle = "rgba(255,0,0,0.25)";
	    ctx.fillRect(mouse.x-mouse.w, mouse.y-mouse.h, mouse.w*2,mouse.h*2);
	}
	for(const e of q.query(mouse)) {
	    ctx.strokeRect(e.p.x-5, e.p.y-5, 10, 10);
	}
    }
    ctx.fillStyle = "#FF0000";
    ctx.fillText(fps.toFixed(2), 0, 10);
};

const applyMouseAction = (q, mouse) => {
    if (mouse.button && (OPTS.debug || OPTS.repulse)) {
        q.queryCall(mouse, (e) => {
            if (OPTS.debug) {
		e.debug = true;
            }
            if (OPTS.repulse) {
                e.repulse(mouse.x, mouse.y);
            }
	});
    }
};

const calcFps = (() => {
    let tacc = 0;
    let fpsAcc = 0;
    let fps = 0;

    return (dt) => {
        tacc += dt;
        fpsAcc++;
        if (tacc > .50) {
            fps  = fpsAcc/tacc;
            fpsAcc = 0;
            tacc = 0;
        }
        return fps;
    };
})();

const calcDt = (() => {
    
    let oldT = Date.now();

    return () => {
	const t = Date.now();
	const dt = (t-oldT)/1000;
	oldT = t;
        return (dt < 1 ? dt : 0);
    };
})();

const mainLoop = (ctx, env, mouse) => {
    const dt = calcDt();

    if (OPTS.run) {
	for(const boid of env.lst) {
	    boid.dir(env.q);
	}
	for(const boid of env.lst) {
	    boid.step(dt);
	}
    }
    applyMouseAction(env.q, mouse);
    draw(ctx, env.q, mouse, calcFps(dt));
    
    window.requestAnimationFrame(() => mainLoop(ctx, env, mouse));
};

window.onload = () => {
    const canvas = document.getElementById("canvas");
    const ctx = canvas.getContext("2d");
    const sprite = makeSprite();

    OPTS.size = parseInt(canvas.width);

    let q = new Quad(OPTS.size/2, OPTS.size/2, OPTS.size/2, OPTS.size/2);
    let lst = genBoids(OPTS.boids, q, sprite);
    let env = { q:q, lst:lst };
    OPTS.env = env; // for debug purposes.

    let mouse = { x: -OPTS.size, y: -OPTS.size, w: 25, h:25 };

    initUi(canvas, mouse, sprite, env);

    mainLoop(ctx, env, mouse);
};
